package com.bibao.noboot.jdbc.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.util.DateUtil;
import com.bibao.noboot.config.JdbcConfig;
import com.bibao.noboot.jdbc.model.Employee;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {JdbcConfig.class})
public class EmployeeDaoTest {
	@Autowired
	private EmployeeDao empDao;
	
	@Test
	public void testFindByName() {
		Employee emp = empDao.findByName("Alice");
		assertEquals(1, emp.getId());
		assertEquals("Alice", emp.getName());
		assertEquals(57200, emp.getSalary());
		assertEquals("1985-05-03", emp.getBirthday().toString());
		Employee emp2 = empDao.findByName("Dummy");
		assertNull(emp2);
	}

	@Test
	public void testSave() {
		int sizeBeforeSave = empDao.findAll().size();
		Employee emp = new Employee();
		emp.setName("Steven");
		emp.setSalary(92700);
		emp.setBirthday(DateUtil.createDate(1978, 3, 27));
		empDao.save(emp);
		int sizeAfterSave = empDao.findAll().size();
		assertEquals(sizeBeforeSave + 1, sizeAfterSave);
	}
}
