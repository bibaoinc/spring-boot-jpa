package com.bibao.noboot.hibernate.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.entity.EmployeeEntity;
import com.bibao.boot.util.DateUtil;
import com.bibao.noboot.config.HibernateConfig;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class})
public class EmployeeDaoTest {
	@Autowired
	private EmployeeDao empDao;
	
	@Test
	public void testFindByName() {
		EmployeeEntity entity = empDao.findByName("Bob");
		assertEquals(2, entity.getId());
		assertEquals("Bob", entity.getName());
		assertEquals(82300, entity.getSalary());
		assertEquals("1982-04-21", entity.getBirthday().toString());
		EmployeeEntity entity2 = empDao.findByName("Dummy");
		assertNull(entity2);
	}

	@Test
	public void testSave() {
		int sizeBeforeSave = empDao.findAll().size();
		EmployeeEntity entity = new EmployeeEntity();
		entity.setName("Tommy");
		entity.setSalary(38500);
		entity.setBirthday(DateUtil.createDate(1992, 8, 13));
		empDao.save(entity);
		int sizeAfterSave = empDao.findAll().size();
		assertEquals(sizeBeforeSave + 1, sizeAfterSave);
	}
}
