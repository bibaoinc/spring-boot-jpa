package com.bibao.boot.repository;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import com.bibao.boot.entity.EmployeeEntity;
import com.bibao.boot.springbootjpa.SpringBootJpaApplication;
import com.bibao.boot.util.DateUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpringBootJpaApplication.class})
public class EmployeeRepositoryDefaultBehaviorTest {
	@Autowired
	private EmployeeRepository empRepo;
	
	@Test
	public void testFindById() {
		EmployeeEntity entity = empRepo.findById(1).get();
		assertEquals(1, entity.getId());
		assertEquals("Alice", entity.getName());
		assertEquals(57200, entity.getSalary());
		assertEquals("1985-05-03", entity.getBirthday().toString());
		Optional<EmployeeEntity> entity2 = empRepo.findById(100);
		assertFalse(entity2.isPresent());
	}

	@Test
	public void testFindAll() {
		List<EmployeeEntity> entities = empRepo.findAll();
		assertFalse(CollectionUtils.isEmpty(entities));
	}
	
	@Test
	public void testSave() {
		EmployeeEntity entity = new EmployeeEntity();
		entity.setName("Linda");
		entity.setSalary(75000);
		entity.setBirthday(DateUtil.createDate(1986, 2, 11));
		empRepo.save(entity);
	}
	
	@Test
	public void testUpdate() {
		EmployeeEntity entity = empRepo.findById(4).get();
		entity.setSalary(entity.getSalary() + 1000);
		empRepo.save(entity);
	}
}
