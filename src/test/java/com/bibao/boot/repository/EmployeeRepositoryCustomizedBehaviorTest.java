package com.bibao.boot.repository;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import com.bibao.boot.entity.EmployeeEntity;
import com.bibao.boot.springbootjpa.SpringBootJpaApplication;
import com.bibao.boot.util.DateUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpringBootJpaApplication.class})
public class EmployeeRepositoryCustomizedBehaviorTest {
	@Autowired
	private EmployeeRepository empRepo;
	
	@Test
	public void testFindByName() {
		EmployeeEntity entity = empRepo.findByName("Bob");
		assertEquals(2, entity.getId());
		assertEquals("Bob", entity.getName());
		assertEquals(82300, entity.getSalary());
		assertEquals("1982-04-21", entity.getBirthday().toString());
	}

	@Test
	public void testFindBySalaryGreaterThan() {
		List<EmployeeEntity> entities = empRepo.findBySalaryGreaterThan(70000);
		assertFalse(CollectionUtils.isEmpty(entities));
		assertEquals(3, entities.size());
		entities.forEach(e -> System.out.println(e.getName() + " | " + e.getSalary()));
	}
	
	@Test
	public void testFindBySalaryGreaterThanAndBirthdayLessThan() {
		List<EmployeeEntity> entities = empRepo.findBySalaryGreaterThanAndBirthdayLessThan(50000, DateUtil.createDate(1983, 0, 1));
		assertFalse(CollectionUtils.isEmpty(entities));
		assertEquals(2, entities.size());
		entities.forEach(e -> System.out.println(e.getName() + " | " + e.getSalary()));
	}
	
	@Test
	public void testFindByBirthday() {
		Date startDate = DateUtil.createDate(1983, 0, 1);
		Date endDate = DateUtil.createDate(1987, 0, 1);
		List<EmployeeEntity> entities = empRepo.findByBirthday(startDate, endDate);
		assertFalse(CollectionUtils.isEmpty(entities));
		assertEquals(2, entities.size());
		entities.forEach(e -> System.out.println(e.getName() + " | " + e.getSalary()));
	}
	
	@Test
	public void testFindAllEmpNames() {
		List<String> empNames = empRepo.findAllEmpNames();
		assertFalse(CollectionUtils.isEmpty(empNames));
		empNames.forEach(name -> System.out.println(name));
	}
	
	private Map<Integer, String> toMap(List<Object[]> objs) {
		Map<Integer, String> map = new HashMap<>();
		objs.forEach(obj -> {
			map.put(((BigDecimal)obj[0]).intValue(), (String)obj[1]);
		});
		return map;
	}
	
	@Test
	public void testFindEmpNameMap() {
		List<Object[]> objs = empRepo.findEmpNameMap();
		Map<Integer, String> nameMap = toMap(objs);
		assertEquals("Alice", nameMap.get(1));
		assertEquals("Bob", nameMap.get(2));
		assertEquals("David", nameMap.get(3));
	}

	@Test
	public void testUpdateSalaryByName() {
		int updatedRecords = empRepo.updateSalaryByName(DateUtil.createDate(1983, 0, 1), 1000);
		assertEquals(2, updatedRecords);
	}
	
}
