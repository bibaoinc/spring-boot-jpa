package com.bibao.boot.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.entity.EmployeeEntity;
import com.bibao.boot.util.DateUtil;
import com.bibao.noboot.config.JpaConfig;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {JpaConfig.class})
public class EmployeeDaoTest {
	@Autowired
	private EmployeeDao empDao;
	
	@Test
	public void testFindById() {
		EmployeeEntity entity = empDao.findById(3);
		assertEquals(3, entity.getId());
		assertEquals("David", entity.getName());
		assertEquals(47700, entity.getSalary());
		assertEquals("1989-10-18", entity.getBirthday().toString());
	}

	@Test
	public void testFindByName() {
		EmployeeEntity entity = empDao.findByName("Bob");
		assertEquals(2, entity.getId());
		assertEquals("Bob", entity.getName());
		assertEquals(82300, entity.getSalary());
		assertEquals("1982-04-21", entity.getBirthday().toString());
	}
	
	@Test
	public void testSaveUpdateDelete() {
		// Save
		int sizeBeforeSave = empDao.findAll().size();
		EmployeeEntity entity = new EmployeeEntity();
		entity.setName("Mary");
		entity.setSalary(76200);
		entity.setBirthday(DateUtil.createDate(1985, 3, 18));
		empDao.save(entity);
		int id = entity.getId();
		int sizeAfterSave = empDao.findAll().size();
		assertEquals(sizeBeforeSave + 1, sizeAfterSave);
		
		// Update
		EmployeeEntity savedEntity = empDao.findById(id);
		assertEquals(76200, savedEntity.getSalary());
		savedEntity.setSalary(78200);
		empDao.update(savedEntity);
		
		EmployeeEntity updatedEntity = empDao.findById(id);
		assertEquals(78200, updatedEntity.getSalary());
		
		// Delete
		empDao.deleteById(id);
		int sizeAfterDelete = empDao.findAll().size();
		assertEquals(sizeAfterSave - 1, sizeAfterDelete);
	}
}
