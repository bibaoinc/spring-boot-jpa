package com.bibao.noboot.hibernate.dao;

import java.util.List;

import com.bibao.boot.entity.EmployeeEntity;

public interface EmployeeDao {
	public void save(EmployeeEntity entity);
	public EmployeeEntity findByName(String name);
	public List<EmployeeEntity> findAll();
}
