package com.bibao.boot.util;

import java.sql.Date;
import java.util.Calendar;

public class DateUtil {
	public static Date createDate(int year, int month, int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, day);
		return new Date(calendar.getTime().getTime());
	}
}
