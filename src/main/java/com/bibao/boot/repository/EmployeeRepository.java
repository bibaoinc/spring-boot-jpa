package com.bibao.boot.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.bibao.boot.entity.EmployeeEntity;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Integer> {
	public EmployeeEntity findByName(String name);
	
	public List<EmployeeEntity> findBySalaryGreaterThan(int salary);  

	public List<EmployeeEntity> findBySalaryGreaterThanAndBirthdayLessThan(int salary, Date birthday);
	
	@Query("select e from EmployeeEntity e where e.birthday between :startDate and :endDate")
	public List<EmployeeEntity> findByBirthday(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "select name from employee", nativeQuery = true)
	public List<String> findAllEmpNames();     

	//@Query(value = "select e.id, e from EmployeeEntity e")
	@Query(value = "select id, name from employee", nativeQuery = true)
	public List<Object[]> findEmpNameMap();
	
	@Modifying
	@Transactional
	@Query("update EmployeeEntity e set e.salary = e.salary + :increasedSalary where e.birthday < :birthday")
	public int updateSalaryByName(@Param("birthday") Date birthday, @Param("increasedSalary") int increasedSalary);

}
